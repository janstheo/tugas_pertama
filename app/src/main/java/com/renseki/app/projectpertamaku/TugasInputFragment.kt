package com.renseki.app.projectpertamaku

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.renseki.app.projectpertamaku.model.Gender
import com.renseki.app.projectpertamaku.model.Login
import com.renseki.app.projectpertamaku.model.Mahasiswa
import kotlinx.android.synthetic.main.second_activity.*

class TugasInputFragment : Fragment(){

    private lateinit var listener: TugasInputActionListener

    companion object {
        fun newInstance(listener: TugasInputActionListener): TugasInputFragment {
            val fragment = TugasInputFragment()
            fragment.listener = listener
            return fragment
        }
    }

    interface TugasInputActionListener {
        fun onLoginReady(login: Login)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        return inflater.inflate(
                R.layout.tugas_input_fragment,
                container,
                false
        )

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        btn_add.setOnClickListener {
            val user = the_real_nrp.text.toString()
            val pass = the_real_full_name.text.toString()

            sendToOtherFragment(user, pass)
        }
    }

    private fun sendToOtherFragment(user: String, pass: String) {
        val mhs = Login(
                user,
                pass
        )
        listener.onLoginReady(mhs)
    }
}